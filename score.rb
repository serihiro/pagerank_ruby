require 'matrix'

class Score
  def initialize(hyper_link_matrix, alpha, residual_erorr = 1e-8)
    @hyper_link_matrix = hyper_link_matrix.is_a?(Matrix) ? hyper_link_matrix : Matrix[hyper_link_matrix]
    unless @hyper_link_matrix.square?
      raise ArgumentError('hyper_link_matrix must be a square matrix')
    end
    @alpha = alpha
    @n = @hyper_link_matrix.row_size
    @residual_erorr = residual_erorr
  end

  def calculate
    pie = Vector[*Array.new(@n, (@n ** (-1)).to_f)]

    # 行列 * 列ベクトルの計算のみで計算するためにgoogle配列の方を転置する。
    # 書籍掲載のMATLAB実装のソースを見ると行列 * ベクトルの計算をする時に
    # この転置処理が入ってないんだけどMATLABは勝手にやってくれるんかね？
    _transposed_google_matrix = google_matrix.transpose

    loop do
      previous_pie = pie
      pie = _transposed_google_matrix * pie
      if pie.norm - previous_pie.norm < @residual_erorr
        break
      end
    end

    pie
  end

  private

  def google_matrix
    return @_google_matrix if @_google_matrix

    ee_t = Matrix.build(@n, @n) { |_, _|1 }
    @_google_matrix = @alpha * stochastic_matrix + (1 - @alpha) * (@n ** (-1)).to_f * ee_t
  end

  def stochastic_matrix
    return @_stochastic_matrix if @_stochastic_matrix

    e_t = Matrix.build(1, @n) { |_, _|1 }
    @_stochastic_matrix = @hyper_link_matrix + (@n ** (-1)).to_f * dangling_node_vector * e_t
  end

  def dangling_node_vector
    return @_dangling_node_vector if @_dangling_node_vector

    dangling_node_array = []
    (0...@n).each do |row|
      dangling_node = true

      (0...@n).each do |column|
        unless @hyper_link_matrix.element(row, column).zero?
          dangling_node = false
          break
        end
      end

      dangling_node_array << if dangling_node
                               [1]
                             else
                               [0]
                             end
    end

    @_dangling_node_vector = Matrix[*(dangling_node_array)]

    @_dangling_node_vector
  end
end

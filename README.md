# What is this?
- This is a reference implementation of PageRank by Ruby.

# References
- Amy N. Langville and Carl D. Meyer. Google’s Pagerank and Beyond: The
Science of Search Engine Rankings. Princeton Univ Pr, 7 2006 (Google PageRankの数理―最強検索エンジンのランキング手法を求めて―, 共立出版 11, 2009)
- 宮嶋健人, [PageRankアルゴリズムおよびそれに関連する研究について](http://www.kentmiyajima.com/pagerank.html), 1 2009

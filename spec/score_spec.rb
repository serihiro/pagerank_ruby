$LOAD_PATH.unshift File.expand_path('../../', __FILE__)
require 'spec_helper'
require 'score.rb'

RSpec.describe Score do
  subject { Score.new(hyper_link_matrix, alpha).calculate }
  let(:alpha) { 0.90 }

  context 'with valid hyper_link_matrix' do
    let(:hyper_link_matrix) do
      Matrix[
        [0, 0.50, 0.50, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0.33, 0.33, 0, 0, 0.33, 0],
        [0, 0, 0, 0, 0.50, 0.50],
        [0, 0, 0, 0.5, 0, 0.50],
        [0, 0, 0, 1.0, 0, 0]
      ]
    end

    it do
      puts subject
    end
  end
end
